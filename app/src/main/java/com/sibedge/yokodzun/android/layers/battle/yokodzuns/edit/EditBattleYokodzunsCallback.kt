package com.sibedge.yokodzun.android.layers.battle.yokodzuns.edit


interface EditBattleYokodzunsCallback {

    fun addYokodzun(yokodzunId: String)

}