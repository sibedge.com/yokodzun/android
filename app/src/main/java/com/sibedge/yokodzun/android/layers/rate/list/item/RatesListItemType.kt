package com.sibedge.yokodzun.android.layers.rate.list.item


enum class RatesListItemType(
    val key: Int
) {

    YOKODZUN_TITLE(key = 0),
    PARAMETER_TITLE(key = 1),
    RATE_ITEM(key = 2)

}